# MindsEditor - CKEditor Build

Custom CKEditor build used at https://www.minds.com/. Forked from https://github.com/ckeditor/ckeditor5-build-balloon.

## Development Installation

To install locally for development, clone the repository and install the packages:

```
# ssh
git clone git@gitlab.com:minds/minds-ckeditor-bundle.git

# or https
git clone https://gitlab.com/minds/minds-ckeditor-bundle.git

# cd into dir and install
cd minds-ckeditor-bundle
npm i
```

To link your local package with the main minds repository, in the ckeditor directory, run
```
npm link
```

Then in your minds/front directory, run
```
npm link @bhayward93/ckeditor5-build-minds
```

This will cause local changes to your editor package to be picked up when rebuilding.

## Development Usage

Make what changes you like, and run:

```
npm run build
```

Your changes should trigger a reload on the front-end if you are serving in development mode.
You can then go to the blog editor in your local Minds instance, and you should see your changes.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please be sure to read our Contribution Guidelines https://developers.minds.com/docs/contributing/contributing/

## License

Forked from https://github.com/ckeditor/ckeditor5-build-balloon, with the license:

Licensed under the terms of [GNU General Public License Version 2 or later](http://www.gnu.org/licenses/gpl.html). For full details about the license, please check the `LICENSE.md` file or [https://ckeditor.com/legal/ckeditor-oss-license](https://ckeditor.com/legal/ckeditor-oss-license).
