/**
 * @license Copyright (c) 2003-2020, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */

/* eslint-env node */

'use strict';

module.exports = {
	extends: 'ckeditor5',
	env: {
		browser: true,
		node: true,
	},
	rules: {
		"no-unused-vars": "off",
		"no-undef": "off"
	}
};
